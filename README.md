# PI camera test

This is the test project to test on the usage of pi camera

## Reference

* [How to display your Raspberry Pi’s desktop on a Mac](https://smittytone.wordpress.com/2016/03/02/mac_remote_desktop_pi/)
* [Getting started with picamera](https://www.raspberrypi.org/learning/getting-started-with-picamera/worksheet/)
* [QR Codes Scanning Distance](https://qrworld.wordpress.com/2011/07/16/qr-codes-scanning-distance/amp/)
* [Measuring Distance from Camera to Target in Image](http://answers.opencv.org/question/51846/measuring-distance-from-camera-to-target-in-image/)
* [How to calculate angle between camera and object](https://stackoverflow.com/questions/30204111/how-to-calculate-angle-between-camera-and-object)
* [Raspberry PI camera configuration](https://www.raspberrypi.org/documentation/configuration/camera.md)